package com.example.zoo.controller;

import com.example.zoo.exception.NameNumberException;
import com.example.zoo.model.Animal;
import com.example.zoo.model.AnimalColor;
import com.example.zoo.service.ZooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZooController {

    private ZooService zooService;

    @Autowired
    public ZooController(ZooService zooService) {
        this.zooService = zooService;
    }

    @GetMapping("/register/{name}/{color}")
    public Animal register(
            @PathVariable("name") String name,
            @PathVariable("color") AnimalColor animalColor) throws NameNumberException {
        return zooService.register(name, animalColor);
    }
}
