package com.example.zoo.exception;

public class NameNumberException extends Exception {
    public NameNumberException(String text) {
        super(text);
    }
}
