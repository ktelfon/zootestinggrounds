package com.example.zoo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NicknameService {

    private RandomService randomService;

    @Autowired
    public NicknameService(RandomService randomService) {
        this.randomService = randomService;
    }

    public String generate(String name) {
        return name+randomService.get();
    }

    // citi metodi
}
