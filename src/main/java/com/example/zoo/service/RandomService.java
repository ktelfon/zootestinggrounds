package com.example.zoo.service;

import org.springframework.stereotype.Service;

@Service
public class RandomService {
    public double get() {
        return Math.random();
    }
}
