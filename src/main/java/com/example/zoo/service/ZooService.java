package com.example.zoo.service;

import com.example.zoo.exception.NameNumberException;
import com.example.zoo.model.Animal;
import com.example.zoo.model.AnimalColor;
import com.example.zoo.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZooService {

    private NicknameService nicknameService;
    private AnimalRepository animalRepository;

    @Autowired
    public ZooService(NicknameService nicknameService, AnimalRepository animalRepository) {
        this.nicknameService = nicknameService;
        this.animalRepository = animalRepository;
    }

    public Animal register(String name, AnimalColor animalColor) throws NameNumberException {

        if(name.matches(".*\\d.*")){
            throw new NameNumberException("No numbers in name");
        }

        Animal newAnimal = new Animal();
        newAnimal.setName(name);
        newAnimal.setColor(animalColor);
        newAnimal.setNickname(nicknameService.generate(name));

        animalRepository.save(newAnimal);
        return newAnimal;
    }
}
