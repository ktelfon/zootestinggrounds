package com.example.zoo.controller;

import com.example.zoo.model.Animal;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class ZooControllerTest {

    @Autowired
    protected MockMvc mvc;

    @BeforeEach
    void setUp() {
    }

    @Test
    void register() throws Exception {
        String animalName = "bim";

        MvcResult mvcResult = mvc.perform(
                MockMvcRequestBuilders.get("/register/"+ animalName+"/BLACK")
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Animal animal = mapFromJson(
                mvcResult.getResponse().getContentAsString(),
                Animal.class);

        assertEquals(animalName,animal.getName());

    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}