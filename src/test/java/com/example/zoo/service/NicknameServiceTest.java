package com.example.zoo.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class NicknameServiceTest {

    NicknameService nicknameService;

    @BeforeEach
    void setUp() {
        RandomService randomService = Mockito.mock(RandomService.class);
        when(randomService.get()).thenReturn(0.1);
        nicknameService = new NicknameService(randomService);
    }

    @Test
    void generate() {
        String expectedName = "G";
        assertEquals(
                expectedName + 0.1,
                nicknameService.generate(expectedName));
    }
}