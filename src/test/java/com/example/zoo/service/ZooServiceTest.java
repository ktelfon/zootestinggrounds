package com.example.zoo.service;

import com.example.zoo.exception.NameNumberException;
import com.example.zoo.model.Animal;
import com.example.zoo.model.AnimalColor;
import com.example.zoo.repository.AnimalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ZooServiceTest {

    ZooService zooService;
    @Mock
    AnimalRepository animalRepository;

    @BeforeEach
    void setUp() {
        NicknameService nicknameService = Mockito.mock(NicknameService.class);

        when(animalRepository.save(any())).then(AdditionalAnswers.returnsFirstArg());

        zooService = new ZooService(nicknameService, animalRepository);
    }

    @Test
    void register() throws NameNumberException {
        String expectedAnimalName = "expectedAnimalName";

        Animal animal = zooService.register(expectedAnimalName, AnimalColor.BLACK);
        assertEquals(expectedAnimalName,animal.getName());
    }
}